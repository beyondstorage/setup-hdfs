# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/)
and this project adheres to [Semantic Versioning](https://semver.org/).

## v0.0.1 - 2021-07-06

Hello, world!

[Unreleased]: https://github.com/beyondstorage/setup-hdfs/compare/v0.0.1...HEAD
